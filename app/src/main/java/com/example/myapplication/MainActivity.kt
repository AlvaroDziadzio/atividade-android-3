package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_ragnar.setOnClickListener {
            changeActivity(ragnarActivity::class.java)
        }

        button_bjorn.setOnClickListener {
            changeActivity(bjornActivity::class.java)
        }

        button_floki.setOnClickListener {
            changeActivity(flokiActivity::class.java)
        }

        button_athelstan.setOnClickListener {
            changeActivity(athelstanActivity::class.java)
        }

        button_lagertha.setOnClickListener {
            changeActivity(lagerthaActivity::class.java)
        }

        button_rollo.setOnClickListener {
            changeActivity(rolloActivity::class.java)
        }

    }

    fun changeActivity(act: Class<*>) {
        val intent = Intent(this, act).putExtra("message", "Você veio do Menu")
        startActivity(intent)
    }
}
