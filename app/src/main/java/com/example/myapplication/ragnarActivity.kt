package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_ragnar.*

class ragnarActivity : AppCompatActivity() {

    lateinit var name: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ragnar)

        name = resources.getString(R.string.name_ragnar)
        Log.d("Activity", "Hello $name")

        text_previous.text = intent.extras["message"] as CharSequence?

        // Buttons

        button_bjorn.setOnClickListener {
            changeActivity(bjornActivity::class.java)
        }

        button_floki.setOnClickListener {
            changeActivity(flokiActivity::class.java)
        }

        button_lagertha.setOnClickListener {
            changeActivity(lagerthaActivity::class.java)
        }

    }

    fun changeActivity(act: Class<*>) {
        Log.d("Activity", "Bye, $name")

        val intent = Intent(this, act).putExtra("message", getString(R.string.app_change_text) + name)
        startActivity(intent)
    }

}
